#!/usr/bin/env python

import yaml
import os
import shutil
import json
import re
from distutils.version import LooseVersion
from jinja2 import Environment, FileSystemLoader

def populate_index_page(template, valid_software):
    output = template.render(valid_software=valid_software)
    with open("output/index.html", 'w') as html_file_pointer:
        html_file_pointer.write(output)

def get_operator(requirement):
    '''
    Splits the operator and version from a requirement string.
    '''
    if requirement == '':
        return '>=', '0'
    m = re.search("\d", requirement)
    pos = m.start()
    if pos == 0:
        return '=', requirement
    check = requirement[:pos].strip()
    ver = requirement[pos:]
    return check, ver

def get_incomplete_gems(final_list):
    unpackaged = ""
    patch = ""
    minor_stable = ""
    minor_devel = ""
    major = ""
    already_newer = ""
    for item in final_list:
        if item['satisfied'] is False:
            if item['version'] == 'NA':
                unpackaged += " - [ ] [%s](https://rubygems.org/gems/%s) | %s <br />" % (
                    item['name'], item['name'], ", ".join(item['requirement']))
            else:
                check, requirement_raw = get_operator(item['violation'])
                required = LooseVersion(requirement_raw)
                if "-" in item['version']:
                    version_raw = item['version'][:item['version'].index('-')]
                if ":" in version_raw:
                    epoch_pos = version_raw.index(":")
                    version_raw = version_raw[epoch_pos + 1:]
                version = LooseVersion(version_raw)
                string_incomplete = " - [ ] [%s](https://rubygems.org/gems/%s) | %s | %s <br />" % (
                    item['name'], item['name'], ", ".join(item['requirement']), version_raw)
                string_complete = " - [x] [%s](https://rubygems.org/gems/%s) | %s | %s <br />" % (
                    item['name'], item['name'], ", ".join(item['requirement']), version_raw)
                if len(required.version) == len(version.version):
                    if len(required.version) == 3:
                        if required.version[0] > version.version[0]:
                            major += string_incomplete
                        elif required.version[0] < version.version[0]:
                            already_newer += string_complete
                        elif required.version[1] != version.version[1]:
                            if required.version[1] < version.version[1]:
                                already_newer += string_complete
                            elif required.version[0] > 0:
                                minor_stable += string_incomplete
                            else:
                                minor_devel += string_incomplete
                        else:
                            if required.version[2] < version.version[2]:
                                already_newer += string_complete
                            else:
                                patch += string_incomplete
                    elif len(required.version) == 2:
                        if required.version[0] > version.version[0]:
                            major += string_incomplete
                        elif required.version[0] < version.version[0]:
                            already_newer += string_complete
                        elif required.version[1] != version.version[1]:
                            if required.version[1] < version.version[1]:
                                already_newer += string_complete
                            elif required.version[0] > 0:
                                minor_stable += string_incomplete
                            else:
                                minor_devel += string_incomplete
                    elif len(required.version) == 1:
                        if required.version[0] > version.version[0]:
                            major += string_incomplete
                        elif required.version[0] < version.version[0]:
                            already_newer += string_complete
                else:
                    min_length = min(len(required.version),
                                     len(version.version))
                    mismatch = 0
                    for position in range(min_length):
                        if required.version[position] != version.version[position]:
                            mismatch = position
                            break
                    if required.version[mismatch] < version.version[mismatch]:
                        already_newer += string_complete
                    else:
                        if mismatch == 0:
                            major += string_incomplete
                        elif mismatch == 1:
                            if required.version[0] > 0:
                                minor_stable += string_incomplete
                            else:
                                minor_devel += string_incomplete
                        elif mismatch == 2:
                            patch += string_incomplete
    output = ""
    if unpackaged != "":
        unpackaged = "**Unpackaged gems** <br />" + unpackaged
        output += "<br />" + unpackaged
    if patch != "":
        patch = "**Patch updates** <br />" + patch
        output += "<br />" + patch
    if minor_stable != "":
        minor_stable = "**Minor updates (Stable)** <br />" + minor_stable
        output += "<br />" + minor_stable
    if minor_devel != "":
        minor_devel = "**Minor updates (Development)** <br />" + minor_devel
        output += "<br />" + minor_devel
    if major != "":
        major = "**Major updates** <br />" + major
        output += "<br />" + major
    if already_newer != "":
        already_newer = "**Already Newer** <br />" + already_newer
        output += "<br />" + already_newer

    return output


def populate_status_pages(template, valid_software):
    ignore_list = ['fog-google', 'mini_portile2', 'newrelic_rpm',
            'newrelic-grape', 'rb-fsevent', 'eco', 'eco-source',
            'gitlab_meta', 'cause', 'rdoc', 'yard']

    color_map = {
            'green': 'table-success',
            'red': 'table-danger',
            'yellow': 'table-warning',
            'cyan': 'table-info',
            'violet': 'table-primary'
            }

    for software, attributes in valid_software.items():
        version = attributes['version']
        print "#### %s-%s ####" % (software, version)

        json_file = "data/%s-%s_debian_status.json" % (software, version)
        with open(json_file, 'r') as json_file_pointer:
            json_file_content = json_file_pointer.read()
            deps = json.loads(json_file_content)

        packaged_count = 0
        unpackaged_count = 0
        itp_count = 0
        total = 0
        mismatch = 0
        final_list = []
        for x, i in deps.items():
            if i['name'] in ignore_list:
                continue
            else:
                final_list.append(i)
                if i['status'] == 'Packaged' or i['status'] == 'NEW':
                    packaged_count += 1
                elif i['status'] == 'ITP':
                    itp_count += 1
                else:
                    unpackaged_count += 1
                if i['satisfied'] is False:
                    mismatch += 1
        total = len(final_list)
        percent_complete = (packaged_count * 100) / total
        output = template.render(
                valid_software=valid_software,
                color_map=color_map,
                software=software,
                version=version,
                final_list=final_list
                )
        with open("output/status/%s-%s.html" % (software, version), 'w') as html_file_pointer:
            html_file_pointer.write(output)

        with open("output/status/%s-%s_todo.html" % (software, version), 'w') as todo_file_pointer:
            todo_file_pointer.write(get_incomplete_gems(final_list))

        print ""

def prepare_dirs():
    shutil.rmtree('output', ignore_errors=True)
    os.makedirs('output/status')

def process():
    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)

    with open('softwares.yml', 'r') as filecontent:
        softwares = yaml.safe_load(filecontent)

    valid_software = {}

    for software, attributes in softwares.items():
        version = attributes['version']
        json_file = "data/%s-%s_debian_status.json" % (software, version)
        if os.path.exists(json_file):
            valid_software[software] = attributes

    prepare_dirs()
    populate_index_page(env.get_template('index.html'), valid_software)
    populate_status_pages(env.get_template('status.html'), valid_software)

process()
